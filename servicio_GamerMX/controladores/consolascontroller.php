<?php
	use gamemx\modelos\Consolas;
	include "modelos\Conexion.php";
	include "modelos\Consolas.php";
	
	class consolascontroller
	{
		function crear()
		{
			if(isset($_POST)){
			$consolas= new \modelos\Consolas();
			$consolas->Nombre=$_POST["Nombre"];
			$consolas->Precio=$_POST["Precio"];
			$consolas->Compania=$_POST["Compania"];
			$consolas->insertar();
			echo '<script>
			alert("Registro Exitoso");
			window.location.href="http://localhost/servicio_GamerMX/vistas/registroconsola.html";
			</script>';
			}
		}
		function buscarID()
		{
			if(isset($_POST)){
				$id_consola = $_POST["id_consola"];
				echo json_encode(\modelos\Consolas::findID($id_consola));
			}
		}
		function buscarNombre()
		{
			if(isset($_POST)){
				$buscar=$_POST['buscar'];
				echo(\modelos\Consolas::findNombre($buscar));
			}
		}
		function buscarTodo()
		{
			echo (\modelos\Consolas::findtodo());
		}
		function eliminar(){
			if(isset($_POST)){
				$eliminar=$_POST["id_consola"];
				json_encode(\modelos\Consolas::eliminarregistro($eliminar));
				echo '<script>
			alert("Eliminado Exitosamente");
			window.location.href="http://localhost/servicio_GamerMX/vistas/eliminarconsola.html";
			</script>';
			}
		}
		function actualizar()
		{
			if(isset($_POST)){
			$consolas= new \modelos\Consolas();
			$consolas->Nombre=$_POST["Nombre"];
			$consolas->Precio=$_POST["Precio"];
			$consolas->Compania=$_POST["Compania"];
			$consolas->id_consola=$_POST["id_consola"];
			$consolas->actualizarregistro();
			echo '<script>
			alert("Modificado Exitosamente");
			window.location.href="http://localhost/servicio_GamerMX/vistas/modificarconsola.html";
			</script>';
			}
		}
	}
?>