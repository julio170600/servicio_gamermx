<?php
	use gamemx\modelos\Juegos;
	include "modelos\Conexion.php";
	include "modelos\Juegos.php";
	
	class juegoscontroller
	{
		function crear()
		{
			if(isset($_POST)){
			$juegos = new \modelos\Juegos();
			$juegos->Nombre=$_POST["Nombre"];
			$juegos->Categoria=$_POST["Categoria"];
			$juegos->Precio=$_POST["Precio"];
			$juegos->Consola=$_POST["Consola"];
			$juegos->insertar();
			echo '<script>
			alert("Registro Exitoso");
			window.location.href="http://localhost/servicio_GamerMX/vistas/registrojuego.html";
			</script>';
			}
		}
		function buscarID()
		{
			if(isset($_POST)){
				$id_juego = $_POST["id_juego"];
				echo json_encode(\modelos\Juegos::findID($id_juego));
			}
		}
		function buscarNombre()
		{
			if(isset($_POST)){
				$buscar=$_POST['buscar'];
				echo(\modelos\Juegos::findNombre($buscar));
			}
		}
		function buscarTodo()
		{
			echo (\modelos\Juegos::findtodo());
		}
		function eliminar(){
			if(isset($_POST)){
				$eliminar=$_POST["id_juego"];
				json_encode(\modelos\Juegos::eliminarregistro($eliminar));
				echo '<script>
				alert("Eliminado Exitosamente");
				window.location.href="http://localhost/servicio_GamerMX/vistas/eliminarjuego.html";
				</script>';
			}
		}
		function actualizar()
		{
			if(isset($_POST)){
			$juegos = new \modelos\Juegos();
			$juegos->Nombre=$_POST["Nombre"];
			$juegos->Categoria=$_POST["Categoria"];
			$juegos->Precio=$_POST["Precio"];
			$juegos->Consola=$_POST["Consola"];
			$juegos->id_juego=$_POST["id_juego"];
			$juegos->actualizarregistro();
			echo '<script>
			alert("Modificado Exitosamente");
			window.location.href="http://localhost/servicio_GamerMX/vistas/modificarjuego.html";
			</script>';
			}
		}
	}
?>