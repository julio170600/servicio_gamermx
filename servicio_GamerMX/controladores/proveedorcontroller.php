<?php
	use gamemx\modelos\Proveedor;
	include "modelos\Conexion.php";
	include "modelos\Proveedor.php";
	
	class proveedorcontroller
	{
		function crear()
		{
			if(isset($_POST)){
			$proveedor = new \modelos\Proveedor();
			$proveedor->Nombre=$_POST["Nombre"];
			$proveedor->Direccion=$_POST["Direccion"];
			$proveedor->Telefono=$_POST["Telefono"];
			$proveedor->Correo=$_POST["Correo"];
			$proveedor->insertar();
			echo '<script>
			alert("Registro Exitoso");
			window.location.href="http://localhost/servicio_GamerMX/vistas/registroproveedor.html";
			</script>';
			}
		}
		function buscarID()
		{
			if(isset($_POST)){
				$id_proveedor = $_POST["id_proveedor"];
				echo json_encode(\modelos\Proveedor::findID($id_proveedor));
			}
		}
		function buscarNombre()
		{
			if(isset($_POST)){
				$buscar=$_POST['buscar'];
				echo (\modelos\Proveedor::findnombre($buscar));
			}
		}
		function buscarTodo()
		{
			echo (\modelos\Proveedor::findtodo());
		}
		function eliminar(){
			if(isset($_POST)){
				$eliminar=$_POST["id_proveedor"];
				json_encode(\modelos\Proveedor::eliminarregistro($eliminar));
				echo '<script>
				alert("Eliminado Exitosamente");
				window.location.href="http://localhost/servicio_GamerMX/vistas/eliminarproveedor.html";
				</script>';
			}
		}
		function actualizar()
		{
			if(isset($_POST)){
			$proveedor = new \modelos\Proveedor();
			$proveedor->Nombre=$_POST["Nombre"];
			$proveedor->Direccion=$_POST["Direccion"];
			$proveedor->Telefono=$_POST["Telefono"];
			$proveedor->Correo=$_POST["Correo"];
			$proveedor->id_proveedor=$_POST["id_proveedor"];
			$proveedor->actualizarregistro();
			echo '<script>
			alert("Modificado Exitosamente");
			window.location.href="http://localhost/servicio_GamerMX/vistas/modificarproveedor.html";
			</script>';
			}
		}
	}
?>