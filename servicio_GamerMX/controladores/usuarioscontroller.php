<?php
	use gamemx\modelos\Usuarios;
	include "modelos\Conexion.php";
	include "modelos\Usuarios.php";
	
	class usuarioscontroller
	{
		function crear()
		{
			if(isset($_POST)){
			$usuarios = new \modelos\Usuarios();
			$usuarios->Usuario=$_POST["Usuario"];
			$usuarios->Password=$_POST["Password"];
			$usuarios->insertar();
			echo '<script>
			alert("Registro Exitoso");
			window.location.href="http://localhost/servicio_GamerMX/vistas/registrousuario.html";
			</script>';
			}
	
		}
		function buscarID()
		{
			if(isset($_POST)){
				$id_usuario = $_POST["id_usuario"];
				echo json_encode(\modelos\Usuarios::findID($id_usuario));
			}
		}
		function buscarUsuario()
		{
			if(isset($_GET)){
				$Usuario = $_GET["Usuario"];
				$Password = $_GET["Password"];
				echo json_encode(["estatus"=>"success","Usuario"=>\modelos\Usuarios::findusuario($Usuario,$Password)]);
			}
		}
		function buscarTodo()
		{
			echo json_encode(\modelos\Usuarios::findtodo());
		}
		function eliminar(){
			if(isset($_POST)){
				$eliminar=$_POST["id_usuario"];
				echo json_encode(\modelos\Usuarios::eliminarregistro($eliminar));
			}
		}
		function actualizar()
		{
			if(isset($_POST)){
			$usuarios = new \modelos\Usuarios();
			$usuarios->Usuario=$_POST["Usuario"];
			$usuarios->Password=$_POST["Password"];
			$usuarios->id_usuario=$_POST["id_usuario"];
			$usuarios->actualizarregistro();
			echo json_encode(["estatus"=>"success","usuarios"=>$usuarios]);
			}
		}
	}
?>