-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-04-2020 a las 10:10:12
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gamersmx`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolas`
--

CREATE TABLE `consolas` (
  `id_consola` int(11) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `Precio` varchar(100) NOT NULL,
  `Compania` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `consolas`
--

INSERT INTO `consolas` (`id_consola`, `Nombre`, `Precio`, `Compania`) VALUES
(3, 'PPSSPP', '$300', 'PlayStation'),
(4, 'PS3', '$3000', 'Sony'),
(6, 'Nintendo 64', '7500', 'Nintendo'),
(8, 'Xbox 360', '$2000', 'Microsoft'),
(12, 'Xbox one', '$6000', 'Microsoft');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `juegos`
--

CREATE TABLE `juegos` (
  `id_juego` int(11) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `Categoria` varchar(300) NOT NULL,
  `Precio` varchar(100) NOT NULL,
  `Consola` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `juegos`
--

INSERT INTO `juegos` (`id_juego`, `Nombre`, `Categoria`, `Precio`, `Consola`) VALUES
(2, 'Halo Reach', 'FPS', '500', 'Xbox One'),
(3, 'Call Of Duty BO3', 'Shoter', '999', 'Play Station 4'),
(5, 'FIFA 20', 'Sport', '299', 'Xbox One'),
(7, 'Spider Man', 'Accion/Aventura', '1000', 'Play Station 4'),
(8, 'Minecraft', 'Aventura', '$350', 'Xbox'),
(9, 'Forza Horizon', 'Carreras', '$800', 'Xbox one'),
(15, 'Spider Man 4', 'Accion', '$100', 'play station 4'),
(16, 'Minecraf', 'Destresa', '$1000', 'XBOX One y Play station 4'),
(17, 'Mario Bros', 'Aventura', '$300', 'Nintendo'),
(18, 'Minecraft', 'Aventura', '$350', 'Xbox'),
(19, 'Spider Man ', 'Accion', '$1000', 'PS4');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `id_proveedor` int(11) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `Direccion` varchar(300) NOT NULL,
  `Telefono` varchar(10) NOT NULL,
  `Correo` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`id_proveedor`, `Nombre`, `Direccion`, `Telefono`, `Correo`) VALUES
(1, 'Activision', 'Tecamac 104', '5578065296', 'Activision@gmail.com'),
(2, 'Electronic Arts', 'Miami', '5548121449', 'EA@gmail.com'),
(3, 'Electronic Arts', 'Miami', '5548121449', 'EA@gmail.com'),
(4, 'Electronic Arts', 'Miami', '5548121449', 'EA@gmail.com'),
(5, 'Electronic Arts', 'Miami', '5548121449', 'EA@gmail.com'),
(6, 'Valve', 'Los Angeles', '2136487214', 'Valve@gmail.com'),
(8, 'Epic Games', 'San Francisco', '5632149752', 'Epic@gmail.com'),
(10, 'Valve', 'Los Angeles', '2136487214', 'Valve@gmail.com'),
(11, 'Epic Games', 'Los angeles', '5536720192', 'epic_games@gmail.com'),
(12, 'Windows', 'av central', '5553021293', 'Windows@outlok.com'),
(13, 'Windows', 'Washington D.C', '0182737280', 'windowa@gmail.com'),
(37, 'Sony', 'Washington D.C', '432917262', 'sonypicturs@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `Usuario` varchar(100) NOT NULL,
  `Password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `Usuario`, `Password`) VALUES
(1, 'julio', '170600'),
(10, 'Miguel00', '170600'),
(11, 'ivonne', '12345'),
(45, 'critian', 'liz'),
(47, 'Maria', '170600'),
(48, 'Gabriel', 'Silva'),
(49, 'critian', 'liz'),
(50, 'emmanuel', 'torres'),
(51, 'emmanuel', 'torres');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `consolas`
--
ALTER TABLE `consolas`
  ADD PRIMARY KEY (`id_consola`);

--
-- Indices de la tabla `juegos`
--
ALTER TABLE `juegos`
  ADD PRIMARY KEY (`id_juego`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `consolas`
--
ALTER TABLE `consolas`
  MODIFY `id_consola` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `juegos`
--
ALTER TABLE `juegos`
  MODIFY `id_juego` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
