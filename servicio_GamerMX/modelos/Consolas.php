<?php
namespace modelos;

class Consolas extends Conexion
{
		public $id_consola;
		public $Nombre;
		public $Precio;
		public $Compania;
		function insertar()
		{
			$pre=mysqli_prepare($this->con,"INSERT INTO consolas(Nombre, Precio, Compania)VALUES(?,?,?)");
			$pre->bind_param("sss",$this->Nombre,$this->Precio,$this->Compania);
			$pre->execute();
			return true;
		}
		static function findID($id_consola)
		{
			$me= new \modelos\Conexion();
			$pre=mysqli_prepare($me->con, "SELECT * FROM consolas WHERE id_consola= ?");
			$pre->bind_param("i", $id_consola);
			$pre->execute();
			$res = $pre->get_result();
			return $res->fetch_object( Consolas::class);
		}
		static function findNombre($buscar)
		{
			if(!empty($buscar)){
			$me= new \modelos\Conexion();
			$query = "SELECT * FROM consolas WHERE Nombre LIKE '$buscar%'";
			$resul=mysqli_query($me->con, $query);
				if(!$resul){
					die('Query Error'.mysqli_error($me->con));
				}
				$json = array();
				while($row = mysqli_fetch_array($resul)){
					$ob = array(
						'Nombre'=> $row['Nombre'],
						'Precio'=>$row['Precio'],
						'Compania'=>$row['Compania'],
						'id_consola'=>$row['id_consola']
					);
					array_push($json, $ob); 
				}
				echo json_encode($json);
				
			}
		}
		static function findtodo()
		{
			$me= new \modelos\Conexion();
			$query = "SELECT * FROM consolas";
			$resul=mysqli_query($me->con, $query);
			if(!$resul){
				die('Query Error'.mysqli_error($me->con));
			}
			$json = array();
			while($row = mysqli_fetch_array($resul)){
				$json[] = array(
						'Nombre'=> $row['Nombre'],
						'Precio'=>$row['Precio'],
						'Compania'=>$row['Compania'],
						'id_consola'=>$row['id_consola']
				);
			}
			echo json_encode($json);
		}
		static function eliminarregistro($id_consola)
		{
			$me= new \modelos\Conexion();
			$pre=mysqli_prepare($me->con, "DELETE FROM consolas WHERE id_consola=?");
			$pre->bind_param("i", $id_consola);
			$pre->execute();
		}
		function actualizarregistro()
		{
			$pre=mysqli_prepare($this->con,"UPDATE consolas SET Nombre=?, Precio=?, Compania=? WHERE id_consola=?");
			$pre->bind_param("sssi",$this->Nombre,$this->Precio,$this->Compania, $this->id_consola);
			$pre->execute();
			return true;
		}
		
}

?>