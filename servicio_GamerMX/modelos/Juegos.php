<?php
namespace modelos;

class Juegos extends Conexion
{
		public $id_juego;
		public $Nombre;
		public $Categoria;
		public $Precio;
		public $Consola;
		function insertar()
		{
			$pre=mysqli_prepare($this->con,"INSERT INTO juegos(Nombre, Categoria, Precio, Consola)VALUES(?,?,?,?)");
			$pre->bind_param("ssss",$this->Nombre,$this->Categoria,$this->Precio,$this->Consola);
			$pre->execute();
			return true;
		}
		static function findID($id_juego)
		{
			$me= new \modelos\Conexion();
			$pre=mysqli_prepare($me->con, "SELECT * FROM juegos WHERE id_juego= ?");
			$pre->bind_param("i", $id_juego);
			$pre->execute();
			$res = $pre->get_result();
			return $res->fetch_object( Juegos::class);
		}
		static function findNombre($buscar)
		{
			if(!empty($buscar)){
			$me= new \modelos\Conexion();
			$query = "SELECT * FROM juegos WHERE Nombre LIKE '$buscar%'";
			$resul=mysqli_query($me->con, $query);
				if(!$resul){
					die('Query Error'.mysqli_error($me->con));
				}
				$json = array();
				while($row = mysqli_fetch_array($resul)){
					$ob = array(
						'Nombre'=> $row['Nombre'],
						'Categoria'=>$row['Categoria'],
						'Precio'=>$row['Precio'],
						'Consola'=>$row['Consola'],
						'id_juego'=>$row['id_juego']
					);
					array_push($json, $ob); 
				}
				echo json_encode($json);
				
			}
		}
		static function findtodo()
		{
			$me= new \modelos\Conexion();
			$query = "SELECT * FROM juegos";
			$resul=mysqli_query($me->con, $query);
			if(!$resul){
				die('Query Error'.mysqli_error($me->con));
			}
			$json = array();
			while($row = mysqli_fetch_array($resul)){
				$json[] = array(
					'Nombre'=> $row['Nombre'],
						'Categoria'=>$row['Categoria'],
						'Precio'=>$row['Precio'],
						'Consola'=>$row['Consola'],
						'id_juego'=>$row['id_juego']
				);
			}
			echo json_encode($json);
		}
		static function eliminarregistro($id_juego)
		{
			$me= new \modelos\Conexion();
			$pre=mysqli_prepare($me->con, "DELETE FROM juegos WHERE id_juego=?");
			$pre->bind_param("i", $id_juego);
			$pre->execute();
		}
		function actualizarregistro()
		{
			$pre=mysqli_prepare($this->con,"UPDATE juegos SET Nombre=?, Categoria=?, Precio=?, Consola=? WHERE id_juego=?");
			$pre->bind_param("ssssi",$this->Nombre,$this->Categoria,$this->Precio,$this->Consola, $this->id_juego);
			$pre->execute();
			return true;
		}
		
}

?>