<?php
namespace modelos;

class Proveedor extends Conexion
{
		public $id_proveedor;
		public $Nombre;
		public $Direccion;
		public $Telefono;
		public $Correo;
		function insertar()
		{
			$pre=mysqli_prepare($this->con,"INSERT INTO proveedor(Nombre, Direccion, Telefono, Correo)VALUES(?,?,?,?)");
			$pre->bind_param("ssss",$this->Nombre,$this->Direccion,$this->Telefono,$this->Correo);
			$pre->execute();
			return true;
		}
		static function findID($id_proveedor)
		{
			$me= new \modelos\Conexion();
			$pre=mysqli_prepare($me->con, "SELECT * FROM proveedor WHERE id_proveedor= ?");
			$pre->bind_param("i", $id_proveedor);
			$pre->execute();
			$res = $pre->get_result();
			return $res->fetch_object( Proveedor::class);
		}
		static function findnombre($buscar)
		{
			if(!empty($buscar)){
			$me= new \modelos\Conexion();
			$query = "SELECT * FROM proveedor WHERE Nombre LIKE '$buscar%'";
			$resul=mysqli_query($me->con, $query);
				if(!$resul){
					die('Query Error'.mysqli_error($me->con));
				}
				$json = array();
				while($row = mysqli_fetch_array($resul)){
					$ob = array(
						'Nombre'=> $row['Nombre'],
						'Direccion'=>$row['Direccion'],
						'Telefono'=>$row['Telefono'],
						'Correo'=>$row['Correo'],
						'id_proveedor'=>$row['id_proveedor']
					);
					array_push($json, $ob); 
				}
				echo json_encode($json);
				
			}
		}
		static function findtodo()
		{
			$me= new \modelos\Conexion();
			$query = "SELECT * FROM proveedor";
			$resul=mysqli_query($me->con, $query);
			if(!$resul){
				die('Query Error'.mysqli_error($me->con));
			}
			$json = array();
			while($row = mysqli_fetch_array($resul)){
				$json[] = array(
						'Nombre'=> $row['Nombre'],
						'Direccion'=>$row['Direccion'],
						'Telefono'=>$row['Telefono'],
						'Correo'=>$row['Correo'],
						'id_proveedor'=>$row['id_proveedor']
				);
			}
			echo json_encode($json);
		}
		static function eliminarregistro($id_proveedor)
		{
			$me= new \modelos\Conexion();
			$pre=mysqli_prepare($me->con, "DELETE FROM proveedor WHERE id_proveedor=?");
			$pre->bind_param("i", $id_proveedor);
			$pre->execute();
		}
		function actualizarregistro()
		{
			$pre=mysqli_prepare($this->con,"UPDATE proveedor SET Nombre=?, Direccion=?, Telefono=?, Correo=? WHERE id_proveedor=?");
			$pre->bind_param("ssssi",$this->Nombre,$this->Direccion,$this->Telefono,$this->Correo, $this->id_proveedor);
			$pre->execute();
			return true;
		}
		
}

?>