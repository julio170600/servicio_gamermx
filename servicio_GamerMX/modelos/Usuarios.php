<?php
namespace modelos;

class Usuarios extends Conexion
{
		public $id_usuario;
		public $Usuario;
		public $Password;

		function insertar()
		{
			$pre=mysqli_prepare($this->con,"INSERT INTO usuarios(Usuario, Password)VALUES(?,?)");
			$pre->bind_param("ss",$this->Usuario,$this->Password);
			$pre->execute();
			return true;
		}
		static function findID($id_usuario)
		{
			$me= new \modelos\Conexion();
			$pre=mysqli_prepare($me->con, "SELECT * FROM usuarios WHERE id_usuario= ?");
			$pre->bind_param("i", $id_usuario);
			$pre->execute();
			$res = $pre->get_result();
			return $res->fetch_object( Usuarios::class);
		}
		static function findusuario($Usuario,$Password)
		{
			$me= new \modelos\Conexion();
			$pre=mysqli_prepare($me->con, "SELECT * FROM usuarios WHERE Usuario=? AND Password=?");
			$pre->bind_param("ss", $Usuario,$Password);
			$pre->execute();
			$res = $pre->get_result();
			if(mysqli_num_rows($res)>0){
				return $res->fetch_object( Usuarios::class);
			}
			
		}
		static function findtodo()
		{
			$me= new \modelos\Conexion();
			$pre=mysqli_prepare($me->con, "SELECT * FROM usuarios");
			$pre->execute();
			$res = $pre->get_result();
			$todo=[];
			while ($tod=$res->fetch_object(Usuarios::class)) {
				array_push($todo,$tod);
			}
			echo json_encode($todo);	
		}
		static function eliminarregistro($id_usuario)
		{
			$me= new \modelos\Conexion();
			$pre=mysqli_prepare($me->con, "DELETE FROM usuarios WHERE id_usuario=?");
			$pre->bind_param("i", $id_usuario);
			$pre->execute();
		}
		function actualizarregistro()
		{
			$pre=mysqli_prepare($this->con,"UPDATE usuarios SET Usuario=?, Password=? WHERE id_usuario=?");
			$pre->bind_param("ssi",$this->Usuario,$this->Password,$this->id_usuario);
			$pre->execute();
			return true;
		}
		
}

?>